// Controls process
#include "Operator.h"

int main(int argc, char* argv[]){
  cout << "a" << endl;
  cout << "MEMO: Inside Operator main;";
  cout << getpid() << endl;
  string args = convcts(argc, argv);
  if(argc > 1){ //Multiple parameters
    //Options?
    if(sb(args,0).compare("new")==0) {
      //Create new instance of Operator.
      Operator op = Operator(sb(args,1),sb(args,2),sb(args,3),sb(args,4),stoi(sb(args,5)));
    }
  } else {
    cerr << "Error: Not enough parameters." << endl;
  }
  
  return 0;
}

Operator::Operator(string idProcessarg, string patharg, string filearg, string lifeTimearg,int position){ //Constructor
  this->path = patharg;
  this->file = filearg;
  this->lifeTime = lifeTimearg;
  this->idProcess = idProcessarg;
  this->position = position;

  if(lifeTime=="0")
    inmortal = true;
  else
    inmortal = false;
  
  cout << "Process: " << idProcess << endl;
  cout << "Operator PID: " << getpid() << endl;
  
  if(!inmortal){ //If mortal
    while(lifeTime.compare("0")!=0){
      cout << "STATUS" << endl;
      cout << "Proceso: " << idProcess
	   << "  vidas: " << lifeTime
	   << endl;
      
      revive();
      lifeTime = to_string(stoi(lifeTime)-1);
    }
  } else { //If inmortal
    cout << "ELSEEEEAAAAAAAA LET IT GOOOO LET IT GOOO" << endl;
    revive();
  }
  exit(EXIT_SUCCESS);
}

Operator::~Operator(){
  
}

void Operator::sumar(string num){
  if(stoi(lifeTime) - stoi(num)>=0){
    this->lifeTime = to_string(stoi(lifeTime) + stoi(num));
  } else {
    cerr << "Error: Invalid operation, program can't have negative lifes." << endl;
  }
}

void Operator::restar(string num){
  if(stoi(lifeTime) - stoi(num)>=0){
    this->lifeTime = to_string(stoi(lifeTime) - stoi(num));
  } else {
    cerr << "Error: Invalid operation, program can't have negative lifes." << endl;
  }
}

void Operator::indef(){
  lifeTime = "0";
  inmortal = true;
}

void Operator::def(int num){
  inmortal = false;
  lifeTime = to_string(num);
}

void Operator::terminate(){
  kill(this->pid, SIGKILL);
  //Statistics?
}

void Operator::interrupt(){
  kill(this->pid, SIGINT);
}

void Operator::restore(){
  kill(this->pid, SIGCONT);
}

void Operator::revive(){
  
  pipe(pin);
  pipe(pout);
  pipe(perr);
  
  pid = vfork();
  if(pid < 0){
    //ERROR
  } else if(pid == 0) { //PROCESO SUICIDA
    close(pin[1]);
    close(pout[0]);
    close(perr[0]);
    char* args[] = {(char *)0};
    string exec = path + "/" + file;
    int err = execv(exec.c_str(),args);
    if(err==-1){
      //ERROR
      cerr << "ERROR: NO EXECUTABLE AT PATH : " << exec << endl;
    }
   _exit(1);
  } else { //Operator
    int status;
    wait(&status);
    close(pin[0]);
    close(pout[1]);
    close(perr[1]);
    cout << "parent process here " << getpid() << endl;
    cout << "son? " << pid << endl;
  }
}

string Operator::getID(){
  return this->idProcess;
}

//MISC FOR SOME RANDOM OPERATIONS...
int Operator::getOpid(){
  return this->pid;
}

int Operator::pidof(string id){
  if(id.compare(this->idProcess)==0){
    return this->pid;
  } else {
    return -2; //RETURNS -2 IF THERE IS NO PID A.K.A PROCESS IS NOT RUNNING.
  }
}

int Operator::getPos(){
  return position;
}

string Operator::getLife(){
  return lifeTime;
}

string convcts(int argc, char* argv[]){
  string result = "";

  for(int i = 1; i<argc; i++){
    result += string(argv[i]);
    if (i!=argc-1) result += " ";
  }

  return result;
}

string sb(string str, int num){
  int cont = 0;
  int len = 0;
  int start = 0;

  for(unsigned int i = 0; i < str.size()+1; i++){
    if(str[i]==' '){
      if(num==cont){
	return str.substr(start,len);
      } else {
	cont++;
	len = 0;
	start = i+1;
      }
    } else {
      len++;
    }
  }
  return str.substr(start,len);
}

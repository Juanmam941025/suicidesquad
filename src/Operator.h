#ifndef OPERATOR_H
#define OPERATOR_H

#include <iostream>    //Standard Input...
#include <stdio.h>     //To delete stuff later...
#include <string>      //F. C strings
#include <fstream>     //Read files...
#include <stdlib.h>    //System, Null...
#include <signal.h>    //For... signal... n stuff...
#include <unistd.h>    //Fork n stuff.
#include <sys/types.h>
#include <sys/wait.h>
//#include <stdio.h>

using namespace std;

class Operator {
 private:
  string idProcess = "";
  string path = "/";
  string file = "";
  string lifeTime = "";
  int position = -1;
  bool active = false;
  bool inmortal = false;
  int pin[2];
  int pout[2];
  int perr[2];
  string getPID(string name);
  
 public:
  pid_t pid = -1;   //Basiclly an int, just with a fancy name...

  Operator(string idProcess,string path, string file, string lifetime,int position);
  ~Operator();
  void sumar(string num);
  void restar(string num);
  void indef();
  void def(int num);

  //Other important stuff...
  void terminate();
  void interrupt();
  void restore();
  void revive();

  //Misc & random stuff...
  string getID();
  int getOpid();
  int pidof(string id);
  string getLife();
  int getPos();
};

string convcts(int argc, char* argv[]);
string sb(string str, int num);

#endif

#include <iostream>   //Standard Input
#include <stdio.h>    //FD stuff..
#include <string.h>
#include <string>     //Who likes C strings anyways?
#include <thread>     //Hilos
#include <fstream>    //Read file.
#include <vector>     //Works as a list n stuff...
#include <signal.h>   //Signal stuff...
#include <stdlib.h>   //System, NULL...    
#include <unistd.h>   //Fork...


using namespace std;

string dirConfFile = "";
static string confFile = "bin/conctrl.cfg";
string input = "";
vector<thread> threads;
int pin[2];
int pout[2];
int perr[2];

//Important functions...
void loadConfigs();
void newOperator(string id, string ruta, string exec, string lifeTime,int pos);
void listar();

string convcts(int argc, char* argv[]);
string sb(string str, int num);

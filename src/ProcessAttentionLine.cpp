// Main class.

#include "ProcessAttentionLine.h"

void newOperator(string id, string path, string file, string lifes, int pos){
  int pid = fork();

  pipe(pin);
  pipe(pout);
  pipe(perr);

  if(pid == 0){
    //Operador
    close(pin[1]);  // Read-Only
    close(pout[0]); // Write-Only
    close(perr[0]); // Write-Only
    
    /** CREATE NEW OPERATOR **/
    
    //I NEED A NEW ARMY... I MEAN, MORE MEMORY.
    char* cid = new char[id.length() + 1];
    char* cpath = new char[path.length() + 1];
    char* cfile = new char[file.length() + 1];
    char* clifes = new char [lifes.length() + 1];
    char* cpos = new char[to_string(pos).length() + 1];
    char* cnew = new char[4];

    //UNCLE SAM NEEDS YOU IN HIS LINES! I MEAN, CHAR* SPACES...
    strcpy(cid, id.c_str());
    strcpy(cpath, path.c_str());
    strcpy(cfile, file.c_str());
    strcpy(clifes, lifes.c_str());
    strcpy(cpos, to_string(pos).c_str());
    strcpy(cnew,"new");

    cout << "MEMO: Inside ProcessAttentionLine; FORK OPERATOR : ";
    cout << pid << endl;
    
    char* args[] = {cfile,cnew,cid,cpath,cfile,clifes,cpos,(char *)0};
    string exec = "bin/procesoctrl";
    int resultado = execv(exec.c_str(),args);

    cout << "Exec : " << resultado << endl;

    //BEE FREEEEEEEE MY MINIONS!! I MEAN... MEMORY...
    delete [] cid;
    delete [] cpath;
    delete [] cfile;
    delete [] clifes;
    delete [] cpos;
    delete [] cnew;

    /** FINALLY CREATED A NEW OPERATOR... HOPEFULLY... **/
    
    cout << "By args : " << id << " "<< path<< " " << file << " " \
	 << lifes << " " <<pos << endl;
    if(resultado == -1){
      cerr << "Error: Could't execute an instance of execl." << endl;
    }
    _exit(1);
  } else {
    //ProcessAttentionLine
    cout << "MEMO: Inside ProcessAttentionLine; FORK SELF : ";
    cout << pid << endl;
    close(pin[0]);
    close(pout[1]);
    close(perr[1]);
  }
}

int main(int argc, char* argv[]){

  cout << "Main pid: " << getpid() << endl;
  
  //INPUT PARAMETERS
  
  if(argc==2){ //Parameter added.
    string nick = convcts(argc,argv);
    if(nick.substr(0,23).compare("--ficheroconfiguracion=")==0){
      cout << "Fichero de configuración en la dependencia "
	   << confFile << "." << endl;
      if(nick.compare(nick.substr(0,23))!=0){
	//Set new confFile
	cout << nick.substr(23) << endl;
      }
    } else if(nick.substr(0,11).compare("--semaforo=")==0){
      cout << "semaforo : " << nick.substr(11) << endl;
    } else if(nick.substr(0,20).compare("--memoriacompartida=")==0){
      cout << "memoria : " << nick.substr(20) << endl;
    } else {
      cout << "Error: Parameter doesn't exist." << endl;
    }
  }

  //END INPUT PARAMETERES
  
  loadConfigs();

  //KIDNAP TERMINAL
  
  while(true){
    cout << "> ";
    getline(cin,input);
    
    //INPUT TERMINAL
    
    if(input.compare("exit")==0){
      exit(EXIT_SUCCESS);
    } else if(sb(input,0).compare("listar")==0){
      cout << "list" << endl;
      listar();
    } else if(sb(input,0).compare("sumar")==0) {
      //Operator* op = findByID(sb(input, 1));
      //if(op)
      //	op->sumar(sb(input, 2));
      //else 
      //cout << "No reference found for : " << sb(input,1)
      //	     << endl;
    } else if(input.substr(0,6).compare("restar")==0) {
      //Operator* op = findByID(sb(input, 1));
      //if(op)
      //op->restar(sb(input, 2));
      //else
      //cout << "No reference found for : " << sb(input,1)
      //     << endl;
    } else if(input.substr(0,9).compare("suspender")==0) {
      //string arg = sb(input,1);
      //Operator* op = findByID(arg);
      //if(op)
      //	op->interrupt();
      //else
      //cout << "No reference found for : " << sb(input,1)
      //     << endl;
    } else if(input.substr(0,11).compare("restablecer")==0){
      //string arg = sb(input,1);
      //Operator* op = findByID(arg);
      //if(op)
      //op->restore();
      //else
      //cout << "No reference found for : " << sb(input,1)
      //     << endl;
    } else if(input.substr(0,9).compare("indefinir")==0){
      //Operator* op = findByID(sb(input,1));
      //if(op)
      //op->indef();
      //else
      //cout <<"No reference found for : " << sb(input,1)
      //     << endl;
    } else if(input.substr(0,7).compare("definir")==0) {
      //Operator* op = findByID(sb(input,1));
      //if(op)
      //op->def(stoi(sb(input,2)));
      //else
      //cout << "No reference found for : " << sb(input,1)
      //     << endl;
    } else if(input.substr(0,8).compare("terminar")==0) {
      //string arg = sb(input,1);
      //Operator* op = findByID(arg);
      //if(op){
      //op->terminate();
      //operators.erase(operators.begin() + op->getPos());
      //} else
      //cout << "No reference found for : " << sb(input,1)
      //     << endl;
    } else {
      cout << "Wut??" << endl;
      //return -1;
    }
    //END INPUT TERMINAL
  }
  //FREE TERMINAL
  
  return 0;
}

/** -------------------------------------------------------------- **/

/**Operator* findByID(string id){
  for(unsigned i = 0; i<operators.size(); i++){
    if(operators.at(i).getID().compare(id)==0){
      //BING! BING! BING!, WE HAVE A WINNER! :D
      return &operators.at(i);
    } else {
      //NOPE, JUST CONTINUE :/
    }
  }
  return nullptr;
  }**/

//PIPE STUFF

//Reads conctrl.cfg and loads the operators to memory.
void loadConfigs(){//Read config file and load new Operators.
  ifstream file(confFile);
  string process = "";
  int pos = 0;
  
  while(getline(file, process)){ //Process stores the info of a single line of the config file.
    string arg = sb(process,0);
    
    if(arg.compare("ProcesoSui")==0){
      //NOTHING
    } else {
      cout << "Error: Grammar error at config file at token " << arg << ", expected ProcesoSui instead." << endl;
    }

    arg = sb(process,2);
    
    if(arg.compare("{")==0){
      //NOTHING
    } else {
      cout << "Error: Grammar error at config file at token " << arg << ", expected { instead." << endl;
    }

    arg = sb(process,4);

    if(arg.compare("::")==0){
      //NOTHING
    } else {
      cout << "Error: Grammar error at config file at token "<< arg << ", expected :: instead." << endl;
    }

    arg = sb(process,7);

    if(arg.compare("}")==0){
      //NOTHING
    } else {
      cout << "Error: Grammar error at config file at token "<< arg << ", expected } instead." << endl;
    }
    
    string id, path, file, lifes;
    id = sb(process,1);
    path = sb(process,3);
    file = sb(process,5);
    lifes = sb(process,6);
    newOperator(id,path,file,lifes,pos);
    //thread{newOperator, id, path, file, lifes, pos};
    //threads.push_back(thread{newOperator, sb(process,1), sb(process,3), sb(process,5), sb(process,6), pos});
    pos++;
  }
    file.close(); //CLOSE FILE
}

//Funcion LISTAR
void listar(){
  
}

string convcts(int argc, char* argv[]){
  string result = "";
  
  for(int i = 1; i<argc; i++){
    result += string(argv[i]);
    if (i!=argc-1) result += " ";
  }
  
  return result;
}

string sb(string str, int num){
  int cont = 0;
  int len = 0;
  int start = 0;

  for(unsigned int i = 0; i < str.size()+1; i++){
    if(str[i]==' '){
      if(num==cont){
	return str.substr(start,len);
      } else {
	cont++;
	len = 0;
	start = i+1;
      }
    } else {
      len++;
    }
  }
  return str.substr(start,len);
}


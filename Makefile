# Some random make file

exec = conctrl
src = src/
bin = bin/
conf = conctrl.cfg

make: clear dependencies compile

compile:
	@echo "Compiling project..."
#	@g++ $(src)Operator.cpp -o Operator.o
#	@g++ -std=gnu++11 -Wall $(src)ProcessAttentionLine.cpp -o $(bin)conctrl
	@g++ -std=gnu++11 -pthread -Wall $(src)ProcessAttentionLine.cpp -o $(bin)conctrl
	@g++ -std=gnu++11 $(src)Operator.cpp -o $(bin)procesoctrl

dependencies:
	@echo "Building project dependencies..."
	@mkdir bin
	@touch $(bin)$(conf)
#Example param.
	@echo "ProcesoSui eternoSuicida { /usr/local/bin :: TendenciasSuicidas 0 }" >> $(bin)$(conf)
	@echo "ProcesoSui pa { /home/juan/Documents/dev/ProcesoSuicidaLinux/src :: ProcesoSuicida 10 }" >> $(bin)$(conf)
	@echo "ProcesoSui pb { /home/juan :: test 3 }" >> $(bin)$(conf)

clear:
	@echo "Cleaning project..."
	@rm -r -f bin
	@rm -f Makefile~
	@rm -f $(src)*.cpp~
	@rm -f $(src)*#
	@rm -f $(src)*.h~
	@rm -f *.xml#
	@rm -f *txt~
